﻿using atm.Domain.Entities;
using atm.Domain.Services;
using atm.Exceptions;
using atm.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace atm.test
{
    [TestClass]
    public class CardServiceTest
    {


        [TestMethod]
        public void CreateCard_ShouldCreateCard_ReturnsBalance5000()
        {
            // Arrange
            ICardService cardService = GetCardService();

            // Act
            var actual = cardService.CreateCard("1234123412341234", 5000);

            // Assert
            Assert.AreEqual(5000, actual.Balance);

        }

        [TestMethod]
        public void CreateCard_ShouldThrow_CardFormatException()
        {
            // Arrange
            ICardService cardService = GetCardService();


            // Assert
            Assert.ThrowsException<CardFormatException>(() =>
            {
                cardService.CreateCard("12341234123", 5000);
            });

        }

        [TestMethod]
        public void GetBalance_Test()
        {
            // Arrange
            ICardService cardService = GetCardService();

            // Act
            var actual = cardService.GetBalance("2222567898765432");

            // Assert
            Assert.AreEqual(2222.22M, actual);

        }

        [TestMethod]
        public void GetBalance_ShouldThrowException()
        {
            // Arrange
            ICardService cardService = GetCardService();
            
            // Assert
            Assert.ThrowsException<CardNotFountException>(()=> {
                cardService.GetBalance("22225678987654");
            });

        }

        [TestMethod]
        public void IsBalanceEnough_ReturnsFalse()
        {
            ICardService cardService = GetCardService();

            var actual = cardService.IsBalanceEnough("2222567898765432", 2223);

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void IsBalanceEnough_ReturnsTrue()
        {
            ICardService cardService = GetCardService();

            var actual = cardService.IsBalanceEnough("1234567898766666", 5000);

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void Withdraw_Test()
        {
            // Arrange
            ICardService cardService = GetCardService();

            // Act
            cardService.Withdraw("1234567898765432", 5000);
            var actual = cardService.GetBalance("1234567898765432");

            // Assert
            Assert.AreEqual(15000, actual);

        }


        private CardService GetCardService()
        {
            List<Card> cards = new List<Card>();

            cards.Add(new Card { CardNumber = "1234567898765432", Balance = 20000.0M });
            cards.Add(new Card { CardNumber = "1234567898765555", Balance = 5000.55M });
            cards.Add(new Card { CardNumber = "1234567898766666", Balance = 4203.10M });
            cards.Add(new Card { CardNumber = "2222567898765432", Balance = 2222.22M });
            cards.Add(new Card { CardNumber = "1234123412341234", Balance = 123.40M });

            return new CardService(new DatabaseService(cards));
        }
    }
}
