﻿using atm.Domain.Entities;
using atm.Domain.Services;
using atm.Exceptions;
using atm.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace atm.test
{
    [TestClass]
    public class ATMachineTest
    {

        [TestMethod]
        public void IsCardInserted_CardExist_ReturnsFalse()
        {
            // Arrange
            IATMachine atm = GetATMachine();

            // Act
            var actual = atm.IsCardInserted();

            // Assert
            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void IsCardInserted_CardExist_ReturnsTrue()
        {
            // Arrange
            IATMachine atm = GetATMachine();
            atm.InsertCard("1234123412341234");
            // Act
            var actual = atm.IsCardInserted();

            // Assert
            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void InsertCard_ShouldThrow_CardFormatException()
        {
            // Arrange
            IATMachine atm = GetATMachine();

            // Assert
            Assert.ThrowsException<CardFormatException>(() => atm.InsertCard(""));
        }

        [TestMethod]
        public void InsertCard_ShouldThrow_CardExistInATMException()
        {
            // Arrange
            IATMachine atm = GetATMachine();

            atm.InsertCard("2222222222222222");

            // Assert
            Assert.ThrowsException<CardExistInATMException>(() => atm.InsertCard("1111111111111111"));
        }



        [TestMethod]
        public void GetCardBalance_ShouldThrow_CardNotExistInATMException()
        {
            IATMachine atm = GetATMachine();

            Assert.ThrowsException<CardNotExistInATMException>(() => atm.GetCardBalance());
        }

        [TestMethod]
        public void GetCardBalance_ShouldThrow_CardNotFountException()
        {
            IATMachine atm = GetATMachine();
            atm.InsertCard("8888888888888888");

            Assert.ThrowsException<CardNotFountException>(() => atm.GetCardBalance());
        }

        [TestMethod]
        public void GetCardBalance_Test()
        {
            IATMachine atm = GetATMachine();
            atm.InsertCard("1234567898766666");

            var actual = atm.GetCardBalance();

            Assert.AreEqual(4203.10M, actual);
        }


        [TestMethod]
        public void GetCardBalance_AfterWithdraw()
        {
            IATMachine atm = GetATMachine();
            atm.InsertCard("1234567898766666");
            atm.WithdrawMoney(900);


            var actual = atm.GetCardBalance();

            // withdraw from card 900 and fee (9)
            decimal expected = 4203.10M - (900+900*0.01M);

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsAmountAllowed_Inputs_999_ReturnsFalse()
        {
            IATMachineValidator atm = GetATMachine();

            var actual = atm.IsAmountAllowed(999);

            Assert.IsFalse(actual);
        }

        [TestMethod]
        public void IsAmountAllowed_Inputs_6345_ReturnsTrue()
        {
            IATMachineValidator atm = GetATMachine();

            var actual = atm.IsAmountAllowed(6345);

            Assert.IsTrue(actual);
        }

        [TestMethod]
        public void WithdrawMoney_ShouldThrow_CardNotExistInATMException()
        {
            IATMachine atm = GetATMachine();

            Assert.ThrowsException<CardNotExistInATMException>(() => atm.WithdrawMoney(960));
        }

        [TestMethod]
        public void WithdrawMoney_ShouldThrow_AmountNotAllowedException()
        {
            IATMachine atm = GetATMachine();
            atm.InsertCard("1234567898766666");

            Assert.ThrowsException<AmountNotAllowedException>(() => atm.WithdrawMoney(11111));
        }

        [TestMethod]
        public void WithdrawMoney_ShouldThrow_BalanceNotEnoughException()
        {
            IATMachine atm = GetATMachine();
            atm.InsertCard("1234567898765555");

            Assert.ThrowsException<BalanceNotEnoughException>(() => atm.WithdrawMoney(5005));
        }

        [TestMethod]
        public void CalculateWithdrawMoney_Input_525()
        {
            IATMachineCalculator atm = GetATMachine();

            var paperNote50 = new PaperNote { Value = 50 };
            var paperNote20 = new PaperNote { Value = 20 };
            var paperNote10 = new PaperNote { Value = 10 };
            var paperNote5 = new PaperNote { Value = 5 };

            Dictionary<PaperNote, int> notes = new Dictionary<PaperNote, int>();
            notes.Add(paperNote50, 10);
            notes.Add(paperNote20, 1);
            notes.Add(paperNote10, 0);
            notes.Add(paperNote5, 1);

            Money money = new Money
            {
                Amount = 525,
                Notes = notes
            };


            var moneyFromATM = atm.CalculateWithdrawMoney(525);


            Assert.AreEqual(money.Amount, moneyFromATM.Amount);
            Assert.AreEqual(money.Notes[paperNote50], moneyFromATM.Notes[paperNote50]);
            Assert.AreEqual(money.Notes[paperNote20], moneyFromATM.Notes[paperNote20]);
            Assert.AreEqual(money.Notes[paperNote10], moneyFromATM.Notes[paperNote10]);
            Assert.AreEqual(money.Notes[paperNote5], moneyFromATM.Notes[paperNote5]);
        }

        [TestMethod]
        public void CalculateWithdrawMoney_Input_6985()
        {
            IATMachineCalculator atm = GetATMachine();

            var paperNote50 = new PaperNote { Value = 50 };
            var paperNote20 = new PaperNote { Value = 20 };
            var paperNote10 = new PaperNote { Value = 10 };
            var paperNote5 = new PaperNote { Value = 5 };

            Dictionary<PaperNote, int> notes = new Dictionary<PaperNote, int>();
            notes.Add(paperNote50, 139);
            notes.Add(paperNote20, 1);
            notes.Add(paperNote10, 1);
            notes.Add(paperNote5, 1);

            Money money = new Money
            {
                Amount = 6985,
                Notes = notes
            };


            var moneyFromATM = atm.CalculateWithdrawMoney(6985);


            Assert.AreEqual(money.Amount, moneyFromATM.Amount);
            Assert.AreEqual(money.Notes[paperNote50], moneyFromATM.Notes[paperNote50]);
            Assert.AreEqual(money.Notes[paperNote20], moneyFromATM.Notes[paperNote20]);
            Assert.AreEqual(money.Notes[paperNote10], moneyFromATM.Notes[paperNote10]);
            Assert.AreEqual(money.Notes[paperNote5], moneyFromATM.Notes[paperNote5]);
        }


        [TestMethod]
        public void WithdrawMoney_Input_830()
        {
            IATMachine atm = GetATMachine();
            atm.InsertCard("1234567898766666");

            var paperNote50 = new PaperNote { Value = 50 };
            var paperNote20 = new PaperNote { Value = 20 };
            var paperNote10 = new PaperNote { Value = 10 };
            var paperNote5 = new PaperNote { Value = 5 };

            Dictionary<PaperNote, int> notes = new Dictionary<PaperNote, int>();
            notes.Add(paperNote50, 16);
            notes.Add(paperNote20, 1);
            notes.Add(paperNote10, 1);
            notes.Add(paperNote5, 0);

            Money money = new Money
            {
                Amount = 830,
                Notes = notes
            };


            var moneyFromATM  = atm.WithdrawMoney(830);


            Assert.AreEqual(money.Amount, moneyFromATM.Amount);
            Assert.AreEqual(money.Notes[paperNote50], moneyFromATM.Notes[paperNote50]);
            Assert.AreEqual(money.Notes[paperNote20], moneyFromATM.Notes[paperNote20]);
            Assert.AreEqual(money.Notes[paperNote10], moneyFromATM.Notes[paperNote10]);
            Assert.AreEqual(money.Notes[paperNote5], moneyFromATM.Notes[paperNote5]);
        }


        [TestMethod]
        public void ReturnCard_ShouldThrow_CardNotExistInATMException()
        {
            IATMachine atm = GetATMachine();
            Assert.ThrowsException<CardNotExistInATMException>(() => atm.ReturnCard());
        }


        [TestMethod]
        public void LoadMoney_Test()
        {
            IATMachine atm = GetATMachine();

            var money = new Money
            {
                Amount = 10960,
                Notes = new Dictionary<PaperNote, int>
                {
                    { new PaperNote{Value = 10}, 1},
                    { new PaperNote{Value = 50}, 219},
                }
            };
            atm.LoadMoney(money);
            Assert.IsTrue(true);
        }


        [TestMethod]
        public void RetrieveChargedFees_Count_2()
        {
            IATMachine atm = GetATMachine();
            atm.InsertCard("1234567898766666");

            atm.WithdrawMoney(830);

            atm.WithdrawMoney(1685);

            var chargedFees = atm.RetrieveChargedFees().ToList();

            Assert.AreEqual(2, chargedFees.Count);
        }

        [TestMethod]
        public void RetrieveChargedFees_TotalFees()
        {
            IATMachine atm = GetATMachine();
            atm.InsertCard("1234567898766666");

            atm.WithdrawMoney(830);

            atm.WithdrawMoney(1685);

            decimal expected = 830 * 0.01M + 1685 * 0.01M;

            var chargedFees = atm.RetrieveChargedFees().ToList();
            decimal actual = 0;

            foreach (var item in chargedFees)
            {
                actual += item.WithdrawalFeeAmount;
            }

            Assert.AreEqual(expected, actual);
        }

        private ATMachine GetATMachine()
        {
            var atmBalance = new Money
            {
                Amount = 28000,
                Notes = new Dictionary<PaperNote, int>
                {
                    { new PaperNote{Value = 5}, 1000},
                    { new PaperNote{Value = 10}, 800},
                    { new PaperNote{Value = 20}, 500},
                    { new PaperNote{Value = 50}, 100},
                }
            };



            List<Card> cards = new List<Card>();

            cards.Add(new Card { CardNumber = "1234567898765432", Balance = 20000.0M });
            cards.Add(new Card { CardNumber = "1234567898765555", Balance = 5000.55M });
            cards.Add(new Card { CardNumber = "1234567898766666", Balance = 4203.10M });
            cards.Add(new Card { CardNumber = "2222567898765432", Balance = 2222.22M });
            cards.Add(new Card { CardNumber = "1234123412341234", Balance = 123.40M });

            var dbService = new DatabaseService(cards);
            var cardService = new CardService(dbService);
            return new ATMachine(cardService, dbService, atmBalance);
        }

    }
}
