﻿using atm.Domain.Entities;
using atm.Domain.Services;
using atm.Exceptions;
using atm.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace atm.test
{
    [TestClass]
    public class DatabaseServiceTest
    {

        [TestMethod]
        public void GetCardByNumber_ShouldThrowException()
        {
            IDatabaseService database = GetDatabase();

            Assert.ThrowsException<CardNotFountException>(() =>
            {
                database.GetCardByNumber("123412341234123");
            });
        }

        [TestMethod]
        public void GetCardByNumber_ShouldReturnsCardWithBalance4203_10M()
        {
            IDatabaseService database = GetDatabase();

            var actual = database.GetCardByNumber("1234567898766666");

            Assert.AreEqual(4203.10M, actual.Balance);
        }


        [TestMethod]
        public void GetChargedFees()
        {
            IDatabaseService database = GetDatabase();
            var fee1 = new Fee
            {
                CardNumber = "",
                WithdrawalDate = DateTime.Now,
                WithdrawalFeeAmount = 1.1M
            };
            var fee2 = new Fee
            {
                CardNumber = "",
                WithdrawalDate = DateTime.Now,
                WithdrawalFeeAmount = 0.9M
            };
            var fee3 = new Fee
            {
                CardNumber = "",
                WithdrawalDate = DateTime.Now,
                WithdrawalFeeAmount = 0.3M
            };

            database.AddChargedFee(fee1);
            database.AddChargedFee(fee2);
            database.AddChargedFee(fee3);
            var actual = database.GetChargedFees().ToList();

            Assert.AreEqual(3, actual.Count);

        }


        private DatabaseService GetDatabase()
        {
            List<Card> cards = new List<Card>();

            cards.Add(new Card { CardNumber = "1234567898765432", Balance = 20000.0M });
            cards.Add(new Card { CardNumber = "1234567898765555", Balance = 5000.55M });
            cards.Add(new Card { CardNumber = "1234567898766666", Balance = 4203.10M });
            cards.Add(new Card { CardNumber = "2222567898765432", Balance = 2222.22M });
            cards.Add(new Card { CardNumber = "1234123412341234", Balance = 123.40M });

            return new DatabaseService(cards);
        }
    }
}
