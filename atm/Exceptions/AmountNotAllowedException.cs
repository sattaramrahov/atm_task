﻿using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Exceptions
{
    public class AmountNotAllowedException : Exception
    {
        public AmountNotAllowedException()
        {

        }
        public AmountNotAllowedException(string message) : base(message)
        {

        }
    }
}
