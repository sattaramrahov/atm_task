﻿using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Exceptions
{
    public class CardNotFountException : Exception
    {
        public CardNotFountException()
        {

        }
        public CardNotFountException(string message) : base(message)
        {

        }
    }
}
