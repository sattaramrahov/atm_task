﻿using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Exceptions
{
    public class CardExistInATMException : Exception
    {
        public CardExistInATMException()
        {

        }

        public CardExistInATMException(string message) : base(message)
        {

        }
    }
}
