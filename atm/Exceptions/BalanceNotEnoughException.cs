﻿using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Exceptions
{
    public class BalanceNotEnoughException : Exception
    {
        public BalanceNotEnoughException()
        {

        }
        public BalanceNotEnoughException(string message) : base(message)
        {

        }
    }
}
