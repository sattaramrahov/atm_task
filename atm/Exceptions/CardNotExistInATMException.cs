﻿using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Exceptions
{
    public class CardNotExistInATMException : Exception
    {
        public CardNotExistInATMException()
        {

        }
        public CardNotExistInATMException(string message) : base(message)
        {

        }
    }
}
