﻿using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Exceptions
{
    public class CardFormatException : Exception
    {
        public CardFormatException()
        {

        }

        public CardFormatException(string message) : base(message)
        {

        }
    }
}
