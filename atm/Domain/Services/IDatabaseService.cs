﻿using atm.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Domain.Services
{
    public interface IDatabaseService
    {
        Card GetCardByNumber(string cardNumber);

        void AddChargedFee(Fee fee);

        IEnumerable<Fee> GetChargedFees();
    }
}
