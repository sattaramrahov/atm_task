﻿using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Domain.Services
{
    public interface IATMachineValidator
    {
        bool IsAmountAllowed(int amount);

    }
}
