﻿using atm.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Domain.Services
{
    public interface ICardService
    {
        Card CreateCard(string cardNumber, decimal balance);
        decimal GetBalance(string cardNumber);

        bool IsBalanceEnough(string cardNumber, decimal totalAmount);

        void Withdraw(string cardNumber, decimal amount);
    }
}
