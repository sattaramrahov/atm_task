﻿using atm.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Domain.Services
{
    public interface IATMachineCalculator
    {
        Money CalculateWithdrawMoney(int amount);

    }
}
