﻿using atm.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Domain.Services
{
    public interface IATMachine
    {

        bool IsCardInserted();

        void InsertCard(string cardNumber);

        decimal GetCardBalance();

        Money WithdrawMoney(int amount);

        void ReturnCard();

        void LoadMoney(Money money);

        IEnumerable<Fee> RetrieveChargedFees();

        void WithdrawFromATM(Money money);
    }
}
