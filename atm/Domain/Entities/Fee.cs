﻿using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Domain.Entities
{
    public struct Fee
    {
        public string CardNumber { get; set; }
        public decimal WithdrawalFeeAmount { get; set; }
        public DateTime WithdrawalDate { get; set; }
    }
}
