﻿using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Domain.Entities
{
    public class Card
    {
        public string CardNumber { get; set; }
        public decimal Balance { get; set; }

    }
}
