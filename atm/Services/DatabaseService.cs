﻿using atm.Domain.Entities;
using atm.Domain.Services;
using atm.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace atm.Services
{

    
    public class DatabaseService : IDatabaseService
    {
        // Placeholder for data from DB.
        private IEnumerable<Card> _cards;
        private IEnumerable<Fee> _chargedFees;

        public DatabaseService(IEnumerable<Card> cards)
        {
            _cards = cards;
            _chargedFees = new List<Fee>();

        }

        public Card GetCardByNumber(string cardNumber)
        {
            var card = _cards.FirstOrDefault(c => c.CardNumber == cardNumber);
            if (card == null)
            {
                throw new CardNotFountException("Card not exist");
            }
            return card;
        }

        public void AddChargedFee(Fee fee)
        {
            _chargedFees = _chargedFees.Append(fee);
        }

        public IEnumerable<Fee> GetChargedFees()
        {
            return _chargedFees;
        }
    }
}
