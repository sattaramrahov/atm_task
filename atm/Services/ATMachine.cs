﻿using atm.Domain.Entities;
using atm.Domain.Services;
using atm.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Services
{
    public class ATMachine : IATMachine, IATMachineValidator, IATMachineCalculator
    {
        private string _cardNumber;
        private readonly ICardService _cardService;
        private readonly IDatabaseService _database;
        private Money _atmBalance;

        public ATMachine(ICardService cardService, IDatabaseService database, Money atmBalance)
        {
            _cardNumber = null;
            _cardService = cardService;
            _database = database;
            _atmBalance = atmBalance;
        }



        public void InsertCard(string cardNumber)
        {
            if (!string.IsNullOrEmpty(this._cardNumber))
            {
                throw new CardExistInATMException();
            }

            if (string.IsNullOrEmpty(cardNumber) || cardNumber.Length != 16)
            {
                throw new CardFormatException($"Card number should be 16 digit");
            }

            _cardNumber = cardNumber;
        }

        public bool IsCardInserted()
        {
            return !string.IsNullOrEmpty(_cardNumber);
        }

        public decimal GetCardBalance()
        {
            if (string.IsNullOrEmpty(_cardNumber))
            {
                throw new CardNotExistInATMException("Card not exits in ATM. Please enter your bank card");
            }
            return _cardService.GetBalance(_cardNumber);
        }

        public Money WithdrawMoney(int amount)
        {
            if (string.IsNullOrEmpty(_cardNumber))
            {
                throw new CardNotExistInATMException("Card not exits in ATM. Please enter your bank card");
            }

            if (!IsAmountAllowed(amount))
            {
                throw new AmountNotAllowedException("Allowed paper notes: 5, 10, 20, 50");
            }


            decimal fee = amount * 0.01M;
            decimal totalAmount = amount + fee;

            if (!_cardService.IsBalanceEnough(_cardNumber, totalAmount))
            {
                throw new BalanceNotEnoughException("Balance not enough to withdraw");
            }

            _cardService.Withdraw(_cardNumber, totalAmount);

            Fee chargedFee = new Fee
            {
                CardNumber = _cardNumber,
                WithdrawalFeeAmount = fee,
                WithdrawalDate = DateTime.Now
            };

            _database.AddChargedFee(chargedFee);

            Money money = CalculateWithdrawMoney(amount);
            return money;
        }

        public bool IsAmountAllowed(int amount)
        {
            return (amount >= 5 && amount % 5 == 0);
        }

        public Money CalculateWithdrawMoney(int amount)
        {
            int initialAmount = amount;

            int countNotes50 = amount / 50;
            amount = amount - (countNotes50 * 50);

            int countNotes20 = amount / 20;
            amount = amount - (countNotes20 * 20);

            int countNotes10 = amount / 10;
            amount = amount - (countNotes10 * 10);

            int countNotes5 = amount / 5;

            Money money = new Money
            {
                Amount = initialAmount,
                Notes = new Dictionary<PaperNote, int>
                {
                    {new PaperNote{Value = 50}, countNotes50 },
                    {new PaperNote{Value = 20}, countNotes20 },
                    {new PaperNote{Value = 10}, countNotes10 },
                    {new PaperNote{Value = 5}, countNotes5 },
                }
            };

            return money;
        }

        public void ReturnCard()
        {
            if (string.IsNullOrEmpty(this._cardNumber))
            {
                throw new CardNotExistInATMException();
            }

            _cardNumber = null;
        }

        public void LoadMoney(Money money)
        {
            _atmBalance.Amount += money.Amount;

            var paper50 = new PaperNote { Value = 50 };
            var paper20 = new PaperNote { Value = 20 };
            var paper10 = new PaperNote { Value = 10 };
            var paper5 = new PaperNote { Value = 5 };

            if (money.Notes.ContainsKey(paper50))
            {
                _atmBalance.Notes[paper50] = money.Notes[paper50];
            }

            if (money.Notes.ContainsKey(paper20))
            {
                _atmBalance.Notes[paper20] = money.Notes[paper20];
            }
            if (money.Notes.ContainsKey(paper10))
            {
                _atmBalance.Notes[paper10] = money.Notes[paper10];
            }
            if (money.Notes.ContainsKey(paper5))
            {
                _atmBalance.Notes[paper5] = money.Notes[paper5];
            }
        }

        public IEnumerable<Fee> RetrieveChargedFees()
        {
            return _database.GetChargedFees();
        }

        public void WithdrawFromATM(Money money)
        {
            _atmBalance.Amount -= money.Amount;

            var paper50 = new PaperNote { Value = 50 };
            var paper20 = new PaperNote { Value = 20 };
            var paper10 = new PaperNote { Value = 10 };
            var paper5 = new PaperNote { Value = 5 };

            if (money.Notes.ContainsKey(paper50))
            {
                _atmBalance.Notes[paper50] -= money.Notes[paper50];
            }

            if (money.Notes.ContainsKey(paper20))
            {
                _atmBalance.Notes[paper20] -= money.Notes[paper20];
            }
            if (money.Notes.ContainsKey(paper10))
            {
                _atmBalance.Notes[paper10] -= money.Notes[paper10];
            }
            if (money.Notes.ContainsKey(paper5))
            {
                _atmBalance.Notes[paper5] -= money.Notes[paper5];
            }
        }
    }
}
