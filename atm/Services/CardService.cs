﻿using atm.Domain.Entities;
using atm.Domain.Services;
using atm.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace atm.Services
{
    public class CardService : ICardService
    {
        private readonly IDatabaseService _database;

        public CardService(IDatabaseService database)
        {
            _database = database;
        }

        public Card CreateCard(string cardNumber, decimal balance)
        {
            if (string.IsNullOrEmpty(cardNumber) || cardNumber.Length != 16)
            {
                throw new CardFormatException("Card number should be 16 digit");
            }
            var card = new Card
            {
                CardNumber = cardNumber,
                Balance = balance
            };

            return card;
        }

        public decimal GetBalance(string cardNumber)
        {
            var card = _database.GetCardByNumber(cardNumber);
            return card.Balance;
        }

        public bool IsBalanceEnough(string cardNumber, decimal totalAmount)
        {
            var card = _database.GetCardByNumber(cardNumber);
            return card.Balance >= totalAmount;
        }

        public void Withdraw(string cardNumber, decimal amount)
        {
            var card = _database.GetCardByNumber(cardNumber);
            card.Balance -= amount;
        }
    }
}
